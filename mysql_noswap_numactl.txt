http://blog.jcole.us/2010/09/28/mysql-swap-insanity-and-the-numa-architecture/
https://github.com/percona/debian-percona-server-5.6/blob/master/scripts/mysqld_safe.sh



apt-get install numactl
grep -n "cmd" < /usr/bin/mysqld_safe

vi /usr/bin/mysqld_safe

::after::
764 cmd="`mysqld_ld_preload_text`$NOHUP_NICENESS"

::add::
#
# Set mysqld's memory interleave policy.
#

if @TARGET_LINUX@ && test $numa_interleave -eq 1
then
  # Locate numactl, ensure it exists.
  if ! my_which numactl > /dev/null 2>&1
  then
    log_error "numactl command not found, required for --numa-interleave"
    exit 1
  # Attempt to run a command, ensure it works.
  elif ! numactl --interleave=all true
  then
    log_error "numactl failed, check if numactl is properly installed"
  fi

  # Launch mysqld with numactl.
  cmd="$cmd numactl --interleave=all"
elif test $numa_interleave -eq 1
then
  log_error "--numa-interleave is not supported on this platform"
  exit 1
fi





