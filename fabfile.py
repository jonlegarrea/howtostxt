# -*- coding: utf-8 -*-
from fabric.api import env, roles, run, sudo
from fabric.contrib.files import exists

# Define sets of servers as roles
#env.roledefs = {'cluster': ['1.1.1.101','1.1.1.102','1.1.1.103','1.1.1.104','1.1.1.105','1.1.1.106']}
env.roledefs = {'cluster': ['192.168.1.33','192.168.1.35']}
# Set the user and key to use for ssh
env.user = 'uster'
env.key_filename = '~/.ssh/id_rsa'

# Set uster sudo password
env.password = '1234'

# Restrict the function to the 'crawler' role
@roles('cluster')
def type():
    run('uname -a')

@roles('cluster')
def update():
    sudo('aptitude update && aptitude upgrade -y && reboot')

@roles('cluster')
def install():
    sudo('aptitude install ')

@roles('cluster')
def reboot():
    sudo('reboot')

@roles('cluster')
def halt():
    sudo('halt')
