#postgresql

aptitude install postgresql

# su - postgres
$ psql template1
template1=# CREATE USER tom WITH PASSWORD 'myPassword';
template1=# CREATE DATABASE jerry;
template1=# GRANT ALL PRIVILEGES ON DATABASE jerry to tom;
template1=# \q



#nginx phppgadmin

aptitude install nginx php5-fpm php5-pgsql

git clone git://github.com/phppgadmin/phppgadmin.git

vi /etc/nginx/sites-available/default

        location ^~ /phppgadmin/ {
                auth_basic            "Login";
                auth_basic_user_file  /var/nginx/passfile;
                location ~ \.php$ {
                        include snippets/fastcgi-php.conf;
                        fastcgi_pass unix:/var/run/php5-fpm.sock;
                }
        }
	    location ~ \.php$ {
                include snippets/fastcgi-php.conf;
                fastcgi_pass unix:/var/run/php5-fpm.sock;
        }
