#!/bin/bash

USER=""
PASSWORD=""
MYSQLDUMP="/usr/bin/mysqldump"
#MYSQL="/usr/bin/mysql"
DATE=`date +%Y-%m-%d`
MYPATH="/tmp/user/"
DESPATH="/home/user/"

mkdir $MYPATH
cd $MYPATH
for DATABASE in db1 db2 db3 db4
do
$MYSQLDUMP --databases $DATABASE -u$USER -p$PASSWORD --routines | bzip2 -9 > "$MYPATH$DATE.$DATABASE.sql.bz2"
done

tar -jcvf $DESPATH$DATE.tar.bz2 -C $MYPATH .

rm $MYPATH*.sql.bz2


