#https://stackoverflow.com/questions/78306968/installing-gcc13-and-g13-in-debian-bookworm-rust-docker-image

https://gcc.gnu.org/pub/gcc/releases/

tar -xf gcc_release.tar.xz
cd gcc_release

chmod u+x ./contrib/download_prerequisites
./contrib/download_prerequisites

./configure --disable-multilib --enable-languages=c,c++
make -j3
sudo make install
sudo update-alternatives --install /usr/bin/gcc gcc /usr/local/bin/gcc 60 --slave /usr/bin/g++ g++ /usr/local/bin/g++
