https://parzibyte.me/blog/2019/06/17/agregar-usuario-base-de-datos-conceder-permisos-postgresql/

sudo -u postgres psql

#create database
create database nombre_bd;
#show database
\l

#create user
create user tu_usuario with encrypted password 'tu_contraseña';
#show user
\du

#privileges database
grant all privileges on database tu_bd to tu_usuario;
#show privileges
\du

#connect database
\c base_de_datos

#privileges tables
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO tu_usuario;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO tu_usuario;
