https://computingforgeeks.com/how-to-install-postgresql-14-on-debian/


#prerequisites
sudo apt update && sudo apt upgrade -y
sudo apt -y install gnupg2 wget vim

#check packages with distro
sudo apt-cache search postgresql | grep postgresql

#add sources from postgresql
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
#update
sudo apt update && sudo apt upgrade -y

# Install the latest version of PostgreSQL.
# If you want a specific version, use 'postgresql-12' or similar instead of 'postgresql':
sudo apt-get -y install postgresql


#change admin user pass
sudo -u postgres psql
\password

