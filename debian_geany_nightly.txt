https://www.geany.org/download/nightly-builds/

sudo mkdir -p /etc/apt/keyrings/
wget -O- "https://geany.nightlybuilds.org/debian/key.asc" | gpg --dearmor | sudo tee /etc/apt/keyrings/geany.gpg > /dev/null
nano /etc/apt/sources.list
deb [signed-by=/etc/apt/keyrings/geany.gpg] http://geany.nightlybuilds.org/debian/bookworm stable main
