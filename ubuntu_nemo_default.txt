#https://itsfoss.com/install-nemo-file-manager-ubuntu/

#install
sudo apt install nemo


#default file manager
xdg-mime default nemo.desktop inode/directory application/x-gnome-saved-search

gsettings set org.gnome.desktop.background show-desktop-icons false

gsettings set org.nemo.desktop show-desktop-icons true

#verify default file manager
xdg-open $HOME


#undo default file manager
xdg-mime default nautilus.desktop inode/directory application/x-gnome-saved-search

gsettings set org.gnome.desktop.background show-desktop-icons true

#uninstall

sudo apt purge nemo nemo*

sudo apt autoremove
