#!/bin/bash

USER=""
PASSWORD=""
DATABASE=""
MYSQLDUMP="/usr/bin/mysqldump"
MYSQL="/usr/bin/mysql"

$MYSQL -Nse 'show tables' $DATABASE -u$USER -p$PASSWORD | while read table; do $MYSQLDUMP --force --opt  --skip-dump-date --user=$USER --password=$PASSWORD $DATABASE $table > "$table.sql"; done
