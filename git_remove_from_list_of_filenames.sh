#!/bin/bash
while read -r a; do echo "git filter-branch --force --index-filter   'git rm --cached --ignore-unmatch \"$a\"'   --prune-empty --tag-name-filter cat -- --all"; done < list_filenames.txt

#https://help.github.com/articles/remove-sensitive-data/
#git push origin --force --all
#git push origin --force --tags


#git filter-branch --force --index-filter 'git rm --cached --ignore-unmatch "FILENAME"' --prune-empty --tag-name-filter cat -- --all
#git push origin --force --all