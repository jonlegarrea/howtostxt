# git lfs
# https://git-lfs.com/
# https://www.atlassian.com/es/git/tutorials/git-lfs

# debian
apt install git-lfs


git lfs install
git lfs track "*.pdf"

git lfs ls-files

#after editing .gitattributes, to make sure that any existing files matching the relevant patterns are converted to LFS.
git add --renormalize .
git add --renormalize --all

#  if files not comming from remote
git lfs pull

# lfs store credentials for the lifetime of an operation
# https://github.com/git-lfs/git-lfs/issues/3318
git config lfs.cachecredentials true
