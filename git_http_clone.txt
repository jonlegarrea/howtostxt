#https://www.kernel.org/pub/software/scm/git/docs/howto/setup-git-server-over-http.html

######### configuration on server to work with:

$git clone http://url/repo.git


on apache file httpd.conf

<Location /repo.git>
 DAV on
 AuthType Basic
 AuthName "Git"
 AuthUserFile .htpass
 Require valid-user
</Location>


####### .htaccess to disable public access on web

<Directory /var/www/repo.git>
      Options Indexes FollowSymLinks MultiViews
      AllowOverride All
      Require all granted
</Directory>

####### refs error 

fatal: repository 'http://url/repo.git/' not found

on /var/www/repo.git

#git update-server-info 
 
and

#chown www-data.www-data /var/www/repo.git -R

######## if ssl - server certificate verification failed
git config   --global http.sslVerify false
git clone http://url/repo.git
git config   --global http.sslVerify true
cd repo
git config http.sslVerify false
