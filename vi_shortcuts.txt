#vi config file - global for all debian/ubuntu system
/etc/vim/vimrc

#vi config file - local for each user 
/home/username/.vimrc

#############################################################
# vi config
#############################################################

#line number
set nu
set nonu

#syntax highlighting
syn on
syn off

#############################################################
# Working with vi
#############################################################

# search 'word'
/word

#block comment
https://dev.to/grepliz/3-ways-to-comment-out-blocks-of-code-in-vi-6j4

Hit CTRL + v to enter visual block mode.
Use j to navigate and block off the beginning of lines 11-17.
Hit SHIFT + i (capital i) to enter insert mode.
Type // and hit ESC
