#https://www.digitalocean.com/community/tutorials/como-instalar-java-con-apt-get-en-ubuntu-16-04-es

sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer

sudo update-alternatives --config java

sudo nano /etc/environment
	JAVA_HOME="/usr/lib/jvm/java-8-oracle"
	PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:$JAVA_HOME/bin"


source /etc/environment
echo $JAVA_HOME
echo $PATH

nano /etc/profile
export PATH=$PATH:$JAVA_HOME/bin
