/*server create repository*/
mkdir ~/git/repo.git
cd ~/git/repo.git
git --bare init
chown root.gituser /var/cache/git/repo.git -R
chmod g+w /var/cache/git/repo.git -R
vi /var/www/viewgit/inc/localconfig.php

/*client side repository*/
git clone user@host:/var/cache/git/repo.git
cd repo
touch README.txt
git add .
git commit -m 'initial commit'
git push origin master
