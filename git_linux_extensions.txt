# https://github.com/gitextensions/gitextensions/wiki/How-To%3A-run-Git-Extensions-on-Linux
# https://github.com/gitextensions/gitextensions/releases/v2.51.05


sudo apt install git kdiff3 mono-complete
cd ~/bin
wget https://github.com/gitextensions/gitextensions/releases/download/v2.51.05/GitExtensions-2.51.05-Mono.zip
unzip GitExtensions-2.51.05-Mono.zip
rm GitExtensions/Plugins/Bitbucket.dll
