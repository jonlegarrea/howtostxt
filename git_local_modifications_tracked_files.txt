git update-index --skip-worktree FILENAME

There’s a matching “–no-skip-worktree” to flip that bit back off.

http://devblog.avdi.org/2011/05/20/keep-local-modifications-in-git-tracked-files/
