1. USB con SystemRescueCD instalado para arrancar.
http://www.sysresccd.org
Cd que sirve para arracar un linux de recuperacion de equipos.
2. En dicho USB tener copiada la imagen de disco replica_debian7.fsa
3. Arrancar la maquina con la version grafica de sysrescd
4. Utilizar gparted para reparticionar el disco duro:
/dev/sda1	ext4		105GB
/dev/sda2	ext4		165GB
/dev/sda3	ntfs		187GB
/dev/sda4	extended	8GB
/dev/sda5	linux-swap	8GB
5. Clonar la primera particion con la imagen de disco:
		fsarchiver restfs /livemnt/boot/replica_debian7.fsa id=0,dest=/dev/sda1
6. Reiniciar y arrancar mediante el menu de sysrescuecd “el linux instalado en disco”
7. Instalar grub en el MBR del disco duro
		grub-install --root-directory=/ /dev/sda
